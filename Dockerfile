#base image
FROM node:12.2.0-alpine
#workdir
WORKDIR /app
#adding node_modules
ENV PATH /app/node_modules/.bin:$PATH
#intall dependencies
COPY package.json /app/package.json
RUN yarn
#starting app
CMD ["yarn", "start"]
