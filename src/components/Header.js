import React, { Component } from "react";
import { Menu, Container, Image, Dropdown } from "semantic-ui-react";
import { Link, withRouter } from "react-router-dom";
import AuthService from "../services/AuthService";
import logo from "../assets/media/hp_logo.png";

const AuthButton = withRouter(({ history }) =>
  localStorage.getItem("token") !== null ? (
    <Menu.Item
      onClick={() => {
        AuthService.signout(() => {
          history.push("/");
        });
      }}
    >
      Logout
    </Menu.Item>
  ) : (
    <Menu.Item as={Link} to="/login">
      Login
    </Menu.Item>
  )
);

export default class Header extends Component {
  render() {
    return (
      <Menu fixed="top" inverted stackable>
        <Container className="menu__container">
          <Menu.Item as={Link} to="/" header>
            <Image size="small" src={logo} />
            Hogwarts Personal Managment System
          </Menu.Item>
          <Menu.Item stackable>
            {localStorage.getItem("token") !== null ? (
              <Dropdown item simple text="Your magic community">
                <Dropdown.Menu>
                  {localStorage.getItem("isStudent") !== null ? (
                    ""
                  ) : (
                    <Dropdown.Item as={Link} to="/personal">
                      All
                    </Dropdown.Item>
                  )}
                  {localStorage.getItem("isStudent") !== null ? (
                    ""
                  ) : (
                    <Dropdown.Item as={Link} to="/personal/staff">
                      Staff
                    </Dropdown.Item>
                  )}
                  <Dropdown.Item as={Link} to="/personal/students">
                    Students
                  </Dropdown.Item>
                </Dropdown.Menu>
              </Dropdown>
            ) : (
              ""
            )}
            <AuthButton />
          </Menu.Item>
        </Container>
      </Menu>
    );
  }
}
