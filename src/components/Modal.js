import React, { Component } from "react";
import { Modal, Header, Image, List } from "semantic-ui-react";
import PersonalCard from "./Card";

export default class InfoModal extends Component {
  render() {
    const {
      name,
      image,
      house,
      dateOfBirth,
      wand,
      patronus,
      ancestry,
      alive
    } = this.props.character;
    const { wood, core, lenght } = wand;
    return (
      <Modal
        dimmer="blurring"
        trigger={<PersonalCard name={name} image={image} house={house} />}
        closeIcon
      >
        <Modal.Content image>
          <Image wrapped size="medium" src={image} />
          <Modal.Description>
            <Header>About</Header>
            <Header as="h2" content={name} />
            <Header
              as="h4"
              content={alive ? "Alive" : "Deceased"}
              icon={alive ? "thumbs up" : "thumbs down"}
            />
            <List>
              {dateOfBirth ? (
                <List.Item>Date of birth: {dateOfBirth}</List.Item>
              ) : (
                ""
              )}
              {house ? <List.Item>House: {house}</List.Item> : ""}
              {patronus ? <List.Item>Patronus: {patronus}</List.Item> : ""}
              {ancestry ? <List.Item>Ancestry : {ancestry}</List.Item> : ""}
              {core || wood || lenght ? (
                <List.Item>
                  Wand:
                  <List bulleted>
                    {wood ? <List.Item>{wood}</List.Item> : ""}
                    {core ? <List.Item>{core}</List.Item> : ""}
                    {lenght ? <List.Item>{lenght}</List.Item> : ""}
                  </List>
                </List.Item>
              ) : (
                ""
              )}
            </List>
          </Modal.Description>
        </Modal.Content>
      </Modal>
    );
  }
}
