import React, { Component } from "react";
import { Card, Icon } from "semantic-ui-react";

export default class PersonalCard extends Component {
  render() {
    const { name, house, image, ...props } = this.props;
    return (
      <Card {...props}>
        <div
          className="card__image"
          style={{ backgroundImage: `url(${image})` }}
        ></div>
        <Card.Content>
          <Card.Header>{name}</Card.Header>
        </Card.Content>
        <Card.Content extra>
          <span>
            <Icon name="magic" />
            From {house !== "" ? house : "unknow"} House
          </span>
        </Card.Content>
      </Card>
    );
  }
}
