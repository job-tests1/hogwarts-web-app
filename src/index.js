import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import "semantic-ui-css/semantic.min.css";
import { BrowserRouter as Router } from "react-router-dom";

function CustomRouter() {
  return (
    <Router>
      <App />
    </Router>
  );
}

ReactDOM.render(<CustomRouter />, document.getElementById("root"));
