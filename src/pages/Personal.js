import React, { Component } from "react";
import request from "superagent";
import InfoModal from "../components/Modal";
import { Grid, Container } from "semantic-ui-react";

const charEndpoint = "http://hp-api.herokuapp.com/api/characters";

export default class Personal extends Component {
  constructor() {
    super();
    this.state = {
      characters: []
    };
  }

  componentDidMount() {
    this.getCharacters();
  }

  componentDidUpdate(prevProps) {
    if (this.props.match.params.group !== prevProps.match.params.group)
      this.getCharacters();
  }

  getCharacters = () => {
    request
      .get(
        `${charEndpoint}${
          this.props.match.params.group
            ? "/" + this.props.match.params.group
            : ""
        }`
      )
      .then(response => {
        this.setState({
          characters: response.body
        });
      });
  };

  render() {
    const { characters } = this.state;
    return (
      <Container className="cards__container">
        <Grid container stackable columns={3}>
          {Object.keys(characters).map(key => {
            return (
              <Grid.Column>
                <InfoModal key={key} character={characters[key]} />
              </Grid.Column>
            );
          })}
        </Grid>
      </Container>
    );
  }
}
