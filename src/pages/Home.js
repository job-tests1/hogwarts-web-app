import React, { Component } from "react";
import { Header, Button, Container } from "semantic-ui-react";
import { Link } from "react-router-dom";
import back from "../assets/media/background.png";
export default class Home extends Component {
  render() {
    return (
      <Container textAlign="center">
        <Header size="huge">Welcome</Header>
        <Header size="large">
          to Hogwarts School of Witchcraft and Wizardry{" "}
        </Header>
        {localStorage.getItem("token") !== null ? (
          ""
        ) : (
          <Button as={Link} to="login">
            LOGIN
          </Button>
        )}
        <div
          className="home__image"
          style={{ backgroundImage: `url(${back})` }}
        ></div>
      </Container>
    );
  }
}
