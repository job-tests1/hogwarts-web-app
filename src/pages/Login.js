import React, { Component } from "react";
import AuthService from "../services/AuthService";
import { Form, Message, Button, Container } from "semantic-ui-react";
import { Redirect } from "react-router-dom";
import back from "../assets/media/hp_logo.png";

const createToken = type => {
  return (
    type +
    Math.random()
      .toString(36)
      .substr(2)
  );
};

const loginEndpoint =
  "http://ec2-18-236-98-152.us-west-2.compute.amazonaws.com:8000/api/v1/challenge/login";

export default class Login extends Component {
  constructor() {
    super();
    this.state = {
      user: "",
      password: "",
      redirectToReferrer: false,
      onLoginError: false
    };
  }

  handleCredentials = e => {
    e.preventDefault();
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  loginAttempt = () => {
    const credentials = {
      user: this.state.user,
      password: this.state.password
    };

    AuthService.authenticate(
      credentials,
      loginEndpoint,
      () => {
        this.setState(() => ({
          redirectToReferrer: true
        }));
      },
      () => {
        this.setState({
          onLoginError: true
        });
      }
    );
  };

  simulateStaff = () => {
    localStorage.setItem("token", createToken("staff-"));
    window.location.replace("/");
  };

  simulateStudent = () => {
    localStorage.setItem("token", createToken("student-"));
    localStorage.setItem("isStudent", true);
    window.location.replace("/");
  };

  render() {
    const { redirectToReferrer, onLoginError } = this.state;
    const { from } = this.props.location.state || { from: { pathname: "/" } };
    if (redirectToReferrer) {
      return <Redirect to={from} />;
    }
    return (
      <Container textAlign="center">
        <div
          className="home__image"
          style={{ backgroundImage: `url(${back})` }}
        ></div>
        <Form
          error={onLoginError === true ? "error" : ""}
          className="login__form"
          onSubmit={this.loginAttempt}
        >
          <Form.Input
            required
            name="user"
            placeholder="your@email.com"
            onChange={this.handleCredentials}
          />
          <Form.Input
            required
            name="password"
            type="password"
            placeholder="password"
            onChange={this.handleCredentials}
          />
          <Message
            error
            header="Not Allowed"
            content="Check your credentials to enter the site"
          />
          <Button>Login</Button>
        </Form>
        <div className="login__temporal">
          <Button onClick={this.simulateStudent}>
            Enter as a Guest Student
          </Button>
          <Button onClick={this.simulateStaff}>Enter as a Guest Staff</Button>
        </div>
      </Container>
    );
  }
}
