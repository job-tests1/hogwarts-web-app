import React, { Component } from "react";
import "./assets/styles/App.css";
import { Switch, Route, Redirect } from "react-router-dom";
import Home from "./pages/Home";
import Login from "./pages/Login";
import Personal from "./pages/Personal";
import Header from "./components/Header";
import Footer from "./components/Footer";
import { Container } from "semantic-ui-react";

const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props =>
      localStorage.getItem("token") !== null ? (
        <Component {...props} />
      ) : (
        <Redirect
          to={{
            pathname: "/login",
            state: {
              from: props.location
            }
          }}
        />
      )
    }
  />
);

class App extends Component {
  render() {
    return (
      <>
        <Header />
        <Container className="app__body">
          <Switch>
            <Route exact path="/" component={Home} />
            <Route exact path="/login" component={Login} />
            <PrivateRoute exact path="/personal" component={ Personal } />
            <PrivateRoute path="/personal/:group" component={ Personal } />
          </Switch>
        </Container>
        <Footer />
      </>
    );
  }
}

export default App;
