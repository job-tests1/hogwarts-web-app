import request from "superagent";

const createToken = type => {
  return (
    type +
    Math.random()
      .toString(36)
      .substr(2)
  );
};

const AuthService = {
  authenticate(credentials, loginEndpoint, done, onError) {
    request
      .post(loginEndpoint)
      .set("Content-Type", "application/json")
      .send(credentials)
      .then(response => {
        const { isStudent, isStaff } = response.body;
        if (isStudent) {
          localStorage.setItem("token", createToken("student-"));
          localStorage.setItem("isStudent", true);
        }
        if (isStaff) {
          localStorage.setItem("token", createToken("staff-"));
        }
        window.location.replace("/");
      })
      .then(done)
      .catch(onError);
  },
  signout(done) {
    window.location.reload();
    localStorage.removeItem("token");
    localStorage.removeItem("isStudent");
    done();
  }
};

export default AuthService;
